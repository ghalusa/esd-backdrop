<?php
/**
 * @file
 *   Control access to site content based on taxonomy, roles and users.
 *
 *
 */

/**
 * Implements hook_config_info().
 */
function tac_lite_config_info() {
  $prefixes = array();

  $prefixes['tac_lite.settings'] = array(
    'label' => t('Settings'),
    'group' => t('TAC Lite'),
  );

  return $prefixes;
}

/**
 * Implementation of hook_perm().
 */
function tac_lite_permission() {
  return array(
    'administer tac_lite' => array(
      'title' => t('administer tac_lite'),
      'description' => t('TODO Add a description for \'administer tac_lite\''),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function tac_lite_menu() {
  global $user;
  $items = array();

  $items['admin/config/people/tac_lite'] = array(
    'title' => 'Access by Taxonomy',
    'description' => "taxonomy-based permissions by tac_lite",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tac_lite_admin_settings'),
    'weight' => 1, // after 'roles' tab
    'access arguments' => array('administer tac_lite'),
  );

  $items['admin/config/people/tac_lite/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
    'access arguments' => array('administer tac_lite'),
  );

  $config_schemes = config_get('tac_lite.settings', 'config_schemes');
  if ($config_schemes === NULL) {
    $config_schemes = array();
  }

  $schemes = tac_lite_schemes();
  for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
    $title = isset($config_schemes[$scheme_id]['name']) ? $config_schemes[$scheme_id]['name'] : "Scheme $scheme_id";
    $items['admin/config/people/tac_lite/scheme_' . $scheme_id] = array(
      'title' => $title,
      'page callback' => 'tac_lite_admin_settings_scheme',
      'page arguments' => array((string)$scheme_id),
      'type' => MENU_LOCAL_TASK,
      'weight' => $scheme_id,
      'access arguments' => array('administer tac_lite'),
    );
  }

  return $items;
}

/**
 * Returns the settings form
 */
function tac_lite_admin_settings($form, &$form_state) {
  $vocabularies = taxonomy_get_vocabularies();

  if (!count($vocabularies)) {
    $form['body'] = array(
      '#type' => 'markup',
      '#markup' => t('You must <a href="!url">create a vocabulary</a> before you can use tac_lite.',
                array('!url' => url('admin/structure/taxonomy/add/vocabulary'))),
    );
    return $form;
  }
  else {
    $options = array();
    foreach ($vocabularies as $vocabulary_name => $vocab) {
      $options[$vocabulary_name] = $vocab->name;
    }

    $form['categories'] = array(
      '#type' => 'select',
      '#title' => t('Vocabularies'),
      '#default_value' => config_get('tac_lite.settings', 'categories'),
      '#options' => $options,
      '#description' => t('Select one or more vocabularies to control privacy.  <br/>Use caution with hierarchical (nested) taxonomies as <em>visibility</em> settings may cause problems on node edit forms.<br/>Do not select free tagging vocabularies, they are not supported.'),
      '#multiple' => TRUE,
      '#required' => TRUE,
    );

    $scheme_options = array();
    // Currently only view, edit, delete permissions possible, so 7
    // permutations will be more than enough.
    for ($scheme_id = 1; $scheme_id < 8; $scheme_id++) {
      $scheme_options[$scheme_id] = $scheme_id;
    }
    $form['schemes'] = array(
      '#type' => 'select',
      '#title' => t('Number of Schemes'),
      '#description' => t('Each scheme allows for a different set of permissions.  For example, use scheme 1 for read-only permission; scheme 2 for read and update; scheme 3 for delete; etc.  Additional schemes increase the size of your node_access table, so use no more than you need.'),
      '#default_value' => tac_lite_schemes(),
      '#options' => $scheme_options,
      '#required' => TRUE,
    );

    $form['rebuild'] = array(
      '#type' => 'checkbox',
      '#title' => t('Rebuild content permissions now'),
      '#default_value' => FALSE, // default false because usually only needed after scheme has been changed.
      '#description' => t('Do this once, after you have fully configured access by taxonomy.'),
      '#weight' => 9,
    );

    $form['#config'] = 'tac_lite.settings';

    $ret = system_settings_form($form);
    // Special handling is required when this form is submitted.
    $ret['#submit'][] = '_tac_lite_admin_settings_submit';
    return $ret;
  }
}

/**
 * This form submit callback ensures that the form values are saved, and also
 * the node access database table is rebuilt.
 * 2008 : Modified by Paulo to be compliant with drupal 6
 */
function _tac_lite_admin_settings_submit($form, &$form_state) {
  $rebuild = $form_state['values']['rebuild'];

  // Rebuild the node_access table.
  if ($rebuild) {
    node_access_rebuild(TRUE);
  }
  else {
    drupal_set_message(t('Do not forget to <a href=!url>rebuild node access permissions</a> after you have configured taxonomy-based access.', array('!url' => url('admin/reports/status/rebuild'))), 'warning');
  }

  // And rebuild menus, in case the number of schemes has changed.
  menu_rebuild();

}

/**
 * Menu callback to create a form for each scheme.
 * @param $scheme_id
 *   The index of the scheme that we will be creating a form for. Passed in as a page argument from the menu.
 */
function tac_lite_admin_settings_scheme($scheme_id) {
  return drupal_get_form('tac_lite_admin_scheme_form', $scheme_id);
}

/**
 * helper function
 */
function _tac_lite_config($scheme_id) {
  $scheme = config_get('tac_lite.settings', 'config_schemes.' . $scheme_id);
  if (!$scheme) {
    $scheme = array(
      'name' => NULL,
      'perms' => array(),
      'unpublished' => FALSE,
    );
  }

  $scheme['term_visibility'] = (isset($scheme['perms']['grant_view']) && $scheme['perms']['grant_view']);
  $scheme['realm'] = 'tac_lite_scheme_' . $scheme_id;

  return $scheme;
}

/**
 * Returns the form for role-based privileges.
 */
function tac_lite_admin_scheme_form($form, $form_state, $scheme_id) {
  $vocabulary_names = config_get('tac_lite.settings', 'categories');
  $roles = user_roles();

  $scheme = _tac_lite_config($scheme_id);
  $form['#tac_lite_config'] = $scheme;
  if (count($vocabulary_names)) {
    $form['config_scheme'][$scheme_id] = array('#tree' => TRUE);
    $form['config_scheme'][$scheme_id]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Scheme name'),
      '#description' => t('A human-readable name for administrators to see. For example, \'read\' or \'read and write\'.'),
      '#default_value' => $scheme['name'],
      '#required' => TRUE,
    );
    // Currently, only view, update and delete are supported by node_access
    $options = array(
      'grant_view' => 'view',
      'grant_update' => 'update',
      'grant_delete' => 'delete',
    );
    $form['config_scheme'][$scheme_id]['perms'] = array(
      '#type' => 'select',
      '#title' => t('Permissions'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $scheme['perms'],
      '#description' => t('Select which permissions are granted by this scheme.  <br/>Note when granting update, it is best to enable visibility on all terms.  Otherwise a user may unknowingly remove invisible terms while editing a node.'),
      '#required' => FALSE,
    );

    $form['config_scheme'][$scheme_id]['unpublished'] = array(
      '#type' => 'checkbox',
      '#title' => t('Apply to unpublished content'),
      '#description' => t('If checked, permissions in this scheme will apply to unpublished content.  If this scheme includes the view permission, then <strong>unpublished nodes will be visible</strong> to users whose roles would grant them access to the published node.'),
      '#default_value' => $scheme['unpublished'],
    );

    $form['config_scheme'][$scheme_id]['term_visibility'] = array(
      '#type' => 'checkbox',
      '#title' => t('Visibility'),
      '#description' => t('If checked, this scheme determines whether a user can view <strong>terms</strong>.  Note the <em>view</em> permission in the select field above refers to <strong>node</strong> visibility.  This checkbox refers to <strong>term</strong> visibility, for example in a content edit form or tag cloud.'),
      '#default_value' => $scheme['term_visibility'],
    );

    $form['helptext'] = array(
      '#type' => 'markup',
      '#markup' => t('To grant to an individual user, visit the <em>access by taxonomy</em> tab on the account edit page.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );
    $form['helptext2'] = array(
      '#type' => 'markup',
      '#markup' => t('To grant by role, select the terms below.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );
    $all_defaults = config_get('tac_lite.settings', 'grants_schemes.' . $scheme_id);
    if ($all_defaults === NULL) {
      $all_defaults = array();
    }
    $form['grants_scheme'][$scheme_id] = array('#tree' => TRUE);
    foreach ($roles as $rid => $role_name) {
      $form['grants_scheme'][$scheme_id][$rid] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => check_plain(t('Grant permission by role: !role', array('!role' => $role_name))),
        '#description' => t(''),
        '#collapsible' => TRUE,
      );

      $defaults = isset($all_defaults[$rid]) ? $all_defaults[$rid] : NULL;
      foreach ($vocabulary_names as $vocabulary_name) {
        // Build a taxonomy select form element for this vocab
        $tree = taxonomy_get_tree($vocabulary_name);
        $options = array(0 => '<' . t('none') . '>');
        if ($tree) {
          foreach ($tree as $term) {
            $choice = new stdClass();
            $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
            $options[] = $choice;
          }
        }
        $default_values = isset($defaults[$vocabulary_name]) ? $defaults[$vocabulary_name] : NULL;
        $form['grants_scheme'][$scheme_id][$rid][$vocabulary_name] = _tac_lite_term_select($vocabulary_name, $default_values);
      }
    }

    $form['rebuild'] = array(
      '#type' => 'checkbox',
      '#title' => t('Rebuild content permissions now'),
      '#default_value' => FALSE, // default false because usually only needed after scheme has been changed.
      '#description' => t('Do this once, after you have fully configured access by taxonomy.'),
      '#weight' => 9,
    );

    $form['#submit'][] = 'tac_lite_admin_scheme_form_submit';
    return system_settings_form($form);
  }
  else {
    $form['help'] = array(
      '#type' => 'markup',
      '#prefix' => '<p>', '#suffix' => '</p>',
      '#markup' => t('First, select one or more vocabularies on the <a href=!url>settings tab</a>. Then, return to this page to complete configuration.', array('!url' => url('admin/config/people/tac_lite/settings'))));
    return $form;
  }
}

/**
 * Submit function for admin settings form to rebuild the menu.
 */
function tac_lite_admin_scheme_form_submit($form, &$form_state) {
  state_set('menu_rebuild_needed', TRUE);

  // Rebuild the node_access table.
  if ($form_state['values']['rebuild']) {
    node_access_rebuild(TRUE);
  }
  else {
    drupal_set_message(t('Do not forget to <a href=!url>rebuild node access permissions</a> after you have configured taxonomy-based access.', array(
                           '!url' => url('admin/reports/status/rebuild'),
                         )), 'warning');
  }
}

/**
 * Implementation of hook_user_categories
 *
 * Creates the user edit category form for tac_lite's user-specific permissions under user/edit
 */
function tac_lite_user_categories() {
  return array(
    array(
      'name' => 'tac_lite',
      'title' => t('Access by taxonomy'),
      'weight' => 5,
      'access callback' => 'user_access',
      'access arguments' => array('administer users'),
    ),
  );
}

/**
 * Implementation of hook_form_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form. The arguments that drupal_get_form() was originally called with are available in the array $form_state['build_info']['args'].
 * @param $form_id
 *   String representing the name of the form itself. Typically this is the name of the function that generated the form.
 *
 */
function tac_lite_form_alter(&$form, &$form_state, $form_id){
  // Catch for the tac_lite category on the user edit form.
  if ($form_id == 'user_profile_form') {
    if ($form['#user_category'] == 'tac_lite') {
      $vocabulary_names = config_get('tac_lite.settings', 'categories');
      if (count($vocabulary_names)) {
        $schemes = tac_lite_schemes();
        for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
          $scheme = _tac_lite_config($scheme_id);
          if ($config['name']) {
            $perms = $config['perms'];
            if ($config['term_visibility']) {
              $perms[] = t('term visibility');
            }
            $form['tac_lite'][$config['realm']] = array(
              '#type' => 'fieldset',
              '#title' => $config['name'],
              '#description' => t('This scheme controls %perms.', array('%perms' => implode(' and ', $perms))),
              '#tree' => TRUE,
            );
            // Create a form element for each vocabulary
            foreach ($vocabulary_names as $vocabulary_name) {
              // TODO: Should we be looking in form_state also for the default value?
              // (Might only be necessary if we are adding in custom validation?)
              $default_values = array();
              if (!empty($form['#user']->data[$config['realm']])) {
                if (isset($form['#user']->data[$config['realm']][$vocabulary_name])) {
                  $default_values = $form['#user']->data[$config['realm']][$vocabulary_name];
                }
              }
              $form['tac_lite'][$config['realm']][$vocabulary_name] =  _tac_lite_term_select($vocabulary_name, $default_values);
              $form['tac_lite'][$config['realm']][$vocabulary_name]['#description'] =
                t('Grant permission to this user by selecting terms.  Note that permissions are in addition to those granted based on user roles.');
            }
          }
        }
        $form['tac_lite'][0] = array(
          '#type' => 'markup',
          '#markup' => '<p>' . t('You may grant this user access based on the schemes and terms below.  These permissions are in addition to <a href="!url">role based grants on scheme settings pages</a>.',
                    array('!url' => url('admin/config/people/tac_lite/scheme_1'))) . "</p>\n",
          '#weight' => -1,
        );
      }
      else {
        // TODO: Do we need to handle the situation where no vocabularies have been set up yet / none have been assigned to tac_lite?
      }
      return $form;
    }
  }
}

/**
 * Implementation of hook_user_presave().
 *
 * Move the tac_lite data into the data object
 * @param $edit
 *   The array of form values submitted by the user.
 * @param $account
 *   The user object on which the operation is performed.
 * @param $category
 *   The active category of user information being edited.
 */
function tac_lite_user_presave(&$edit, $account, $category) {
  // Only proceed if we are in the tac_lite category.
  if ($category == 'tac_lite'){
    // Go through each scheme and copy the form value into the data element
    $schemes = tac_lite_schemes();
    for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
      $config = _tac_lite_config($scheme_id);
      if ($config['name']) {
        $edit['data'][$config['realm']] = $edit[$config['realm']];
      }
    }
  }
}

/**
 * Implements hook_node_access_records().
 *
 * We are given a node and we return records for the node_access table.  In
 * our case, we inpect the node's taxonomy and grant permissions based on the
 * terms.
 */
function tac_lite_node_access_records($node) {
  // Get the tids we care about that are assigned to this node
  $tids = _tac_lite_get_terms($node);

  if (!count($tids)) {
    // no relevant terms found.

    // in drupal 4-7 we had to write a row into the database. In drupal 5 and later, it should be safe to do nothing.
  }
  else {
    // if we're here, the node has terms associated with it which restrict
    // access to the node.
    $grants = array();
    $schemes = tac_lite_schemes();
    for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
      $config = _tac_lite_config($scheme_id);
      // Only apply grants to published nodes, or unpublished nodes if requested in the scheme
      if ($node->status || $config['unpublished']) {
        foreach ($tids as $tid) {
          $grant = array(
            'realm' => $config['realm'],
            'gid' => $tid, // use term id as grant id
            'grant_view' => 0,
            'grant_update' => 0,
            'grant_delete' => 0,
            'priority' => 0,
          );
          foreach ($config['perms'] as $perm) {
            $grant[$perm] = TRUE;
          }
          $grants[] = $grant;
        }
      }
    }
    return $grants;
  }
}

/**
 * Gets terms from a node that belong to vocabularies selected for use by tac_lite
 *
 * @param $node
 *   A node object
 * @return
 *   An array of term ids
 */
function _tac_lite_get_terms($node) {
  $tids = array();

  // Get the vocabulary_names that tac_lite cares about.
  $vocabulary_names = config_get('tac_lite.settings', 'categories');
  if ($vocabulary_names) {
    // Load all terms found in term reference fields.
    // This logic should work for all nodes (published or not).
    $terms_by_vocabulary_name = tac_lite_node_get_terms($node);
    if (!empty($terms_by_vocabulary_name)) {
      foreach ($vocabulary_names as $vocabulary_name) {
        if (!empty($terms_by_vocabulary_name[$vocabulary_name])) {
          foreach ($terms_by_vocabulary_name[$vocabulary_name] as $tid => $term) {
            $tids[$tid] = $tid;
          }
        }
      }
    }

    // The logic above should have all terms already, but just in case we use
    // the "original" logic below.  The taxonomy module stopped writing to the
    // taxonomy_index for unpublished nodes, so this works only for published
    // nodes.
    $query = db_select('taxonomy_index', 'r');
    $t_alias = $query->join('taxonomy_term_data', 't', 'r.tid = t.tid');
    $query->fields( $t_alias );
    $query->condition('r.nid', $node->nid);
    $query->condition('t.vocabulary', $vocabulary_names, 'IN');
    $result = $query->execute();
    foreach ($result as $term) {
      if (empty($tids[$term->tid])) {
        watchdog('tac_lite', 'Unexpected term id %tid associated with !node.  Please report this to !url.', array(
            '%tid' => $term->tid,
            '!node' => l($node->title, 'node/' . $node->nid),
            '!url' => 'https://drupal.org/node/1918272',
          ), WATCHDOG_DEBUG);
      }
      $tids[$term->tid] = $term->tid;
    }
  }
  elseif (user_access('administer tac_lite')) {
    drupal_set_message(t('tac_lite.module enabled, but not <a href=!admin_url>configured</a>. No tac_lite terms associated with %title.', array(
                           '!admin_url' => url('admin/config/people/tac_lite'),
                           '%title' => $node->title,
                         )));
  }

  return $tids;
}

/**
 * In Drupal 6.x, there was taxonomy_node_get_terms().  Drupal 7.x should
 * provide the same feature, but doesn't.  Here is our workaround, based on
 * https://drupal.org/comment/5573176#comment-5573176.
 *
 * We organize our data structure by vocabulary_name and tid.
 */
function tac_lite_node_get_terms($node) {
  $terms = &drupal_static(__FUNCTION__);

  if (!isset($terms[$node->nid])) {
    // Get tids from all taxonomy_term_reference fields.
    $fields = field_info_fields();
    foreach ($fields as $field_name => $field) {
      // Our goal is to get all terms, regardless of language, associated with the node.  Does the code below do that?
      if ($field['type'] == 'taxonomy_term_reference' && field_info_instance('node', $field_name, $node->type)) {
        if (($items = field_get_items('node', $node, $field_name)) && is_array($items)) {
          foreach ($items as $item) {
            // Sometimes $item contains only tid, sometimes entire term.  Thanks Drupal for remaining mysterious!
            // We need to term to determine the vocabulary id.
            if (!empty($item['taxonomy_term'])) {
              $term = $item['taxonomy_term'];
            }
            else {
              $term = taxonomy_term_load($item['tid']);
            }
            if ($term) {
              $terms[$node->nid][$term->vocabulary][$term->tid] = $term;
            }
          }
        }
      }
    }
  }

  return isset($terms[$node->nid]) ? $terms[$node->nid] : FALSE;
}

/**
 * Helper function to build a taxonomy term select element for a form.
 *
 * @param $v
 *   A vocabulary object containing a vocabulary_name and name.
 * @param $default_values
 *   An array of values to use for the default_value argument for this form element.
 */
function _tac_lite_term_select($vocabulary_name, $default_values = array()) {
  $v = taxonomy_vocabulary_load($vocabulary_name);
  $tree = taxonomy_get_tree($vocabulary_name);
  $options = array(0 => '<' . t('none') . '>');
  if ($tree) {
    foreach ($tree as $term) {
      $choice = new stdClass();
      $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
      $options[] = $choice;
    }
  }
  $field_array = array(
    '#type' => 'select',
    '#title' => $v->name,
    '#default_value' => $default_values,
    '#options' => $options,
    '#multiple' => TRUE,
    '#description' => $v->description,
  );
  return $field_array;
}

/**
 * Return the term ids of terms this user is allowed to access.
 *
 * Users are granted access to terms either because of who they are,
 * or because of the roles they have.
 */
function _tac_lite_user_tids($account, $scheme_id) {
  // grant id 0 is reserved for nodes which were not given a grant id when they were created. By adding 0 to the grant id, we let the user view those nodes.
  $grants = array(0);
  $config = _tac_lite_config($scheme_id);
  $realm = $config['realm'];
  if (isset($account->data[$realm]) && count($account->data[$realm])) {
    // $account->$realm is array. Keys are vocabulary_names, values are array of tids within that vocabulary, to which the user has access
    foreach ($account->data[$realm] as $tids) {
      if (count($tids)) {
        $grants = array_merge($grants, $tids);
      }
    }
  }

  // add per-role grants in addition to per-user grants
  $defaults = config_get('tac_lite.settings', 'grants_scheme.' . $scheme_id);
  if (!$defaults) {
    $defaults = array();
  }
  foreach ($account->roles as $rid => $role_name) {
    if (isset($defaults[$rid]) && count($defaults[$rid])) {
      foreach ($defaults[$rid] as $tids) {
        if (count($tids)) {
          $grants = array_merge($grants, $tids);
        }
      }
    }
  }

  // Because of some flakyness in the form API and the form we insert under
  // user settings, we may have a bogus entry with vocabulary_name set
  // to ''. Here we make sure not to return that.
  unset($grants['']);

  return $grants;
}

/**
 * Implementation of hook_node_grants().
 *
 * Returns any grants which may give the user permission to perform the
 * requested op.
 */
function tac_lite_node_grants($account, $op) {
  $grants = array();
  $schemes = tac_lite_schemes();
  for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
    $config = _tac_lite_config($scheme_id);
    if (in_array('grant_' . $op, $config['perms'])) {
      $grants[$config['realm']] = _tac_lite_user_tids($account, $scheme_id);
    }
  }
  if (count($grants)) {
    return $grants;
  }
}

/**
 * Implements hook_query_TAG_alter().
 *
 * Acts on queries that list terms (generally these should be tagged with 'term_access')
 * to remove any terms that this user should not be able to see.
 */
function tac_lite_query_term_access_alter(QueryAlterableInterface $query) {
  global $user;

  // If this user has administer rights, don't filter
  if (user_access('administer tac_lite')) {
    return;
  }

  // Get our vocabularies and schemes from configuration. Return if we have none.
  $vocabulary_names = config_get('tac_lite.settings', 'categories');
  $schemes = tac_lite_schemes();
  if (!$vocabulary_names || !count($vocabulary_names) || !$schemes) {
    return;
  }

  // the terms this user is allowed to see
  $term_visibility = FALSE;
  $tids = array();
  for ($scheme_id = 1; $scheme_id <= $schemes; $scheme_id++) {
    $config = _tac_lite_config($scheme_id);
    if ($config['term_visibility']) {
      $tids = array_merge($tids, _tac_lite_user_tids($user, $scheme_id));
      $term_visibility = TRUE;
    }
  }

  if ($term_visibility) {
    // HELP: What is the proper way to find the alias of the primary table here?
    $primary_table = '';
    $t = $query->getTables();
    foreach($t as $key => $info) {
      if (!$info['join type']) {
        $primary_table = $info['alias'];
      }
    }

    // Prevent query from finding terms the current user does not have permission to see.
    $query->leftJoin('taxonomy_term_data', 'tac_td', $primary_table . '.tid = tac_td.tid');
    $or = db_or();
    $or->condition($primary_table . '.tid', $tids, 'IN');
    $or->condition('tac_td.vocabulary_name', $vocabulary_names, 'NOT IN');
    $query->condition($or);
  }
}

/**
 * Helper function to return number of schemes
 */
function tac_lite_schemes() {
  $schemes = config_get('tac_lite.settings', 'schemes');
  if (!$schemes) {
    $schemes = 1;
  }
  return $schemes;
}
