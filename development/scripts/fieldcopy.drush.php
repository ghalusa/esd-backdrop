<?php
global $user;
if (empty($user->uid)) {
  echo 'be someone to run this', "\n";
  return;
}
echo 'hello ', $user->name, "\n";

$infield = drush_shift();
if (empty($infield)) {
  echo 'no infield', "\n";
  return;
}
echo 'copy from ', $infield, "\n";

$outfield = drush_shift();
if (empty($outfield)) {
  echo 'no outfield', "\n";
  return;
}
echo 'copy to ', $outfield, "\n";

$infield_info = field_info_field($infield);
if (!$infield_info) {
  echo 'field not found: ', $infield, "\n";
  return;
}

$outfield_info = field_info_field($outfield);
if (!$outfield_info) {
  echo 'outfield not found: ', $outfield, "\n";
  return;
}

$common = array();
foreach ($infield_info['bundles'] as $entity_type => $bundles) {
  if (empty($outfield_info['bundles'][$entity_type])) {
    continue;
  }
  $common[$entity_type] = array_intersect($bundles, $outfield_info['bundles'][$entity_type]);
}

foreach ($common as $entity_type => $bundles) {
  foreach ($bundles as $bundle) {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', $entity_type)
      ->entityCondition('bundle', $bundle)
      ->execute()
    ;
    foreach ($result as $et => $rows) {
      $entities = entity_load($et, array_keys($rows));
      foreach ($entities as $entity_id => $entity) {
        $wrapper = entity_metadata_wrapper($et, $entity);
        $value = $wrapper->{$infield}->value();
        $wrapper->{$outfield} = $value;
        echo 'copy ', $et, ' ', $bundle, ' ', $infield, ' to ', $outfield, "\n";
        $wrapper->save();
      }
    }
  }
}
