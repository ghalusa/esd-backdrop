<?php

$nid = 147;
$nids = array();
$result = db_query("select nid from node where type = 'result'");
foreach ($result as $row) {
  $nids[] = $row->nid;
}
foreach ($nids as $nid) {
  copypub($nid);
}

function copypub($nid) {
  $node = node_load($nid);
  // print_r($node);
  $pubids = array();
  if (!empty($node->field_publication_ps[LANGUAGE_NONE])) {
    foreach ($node->field_publication_ps[LANGUAGE_NONE] as $delta => $row) {
      if (!empty($row['value'])) {
        $pubids[] = $row['value'];
      }
    }
  }
  if ($pubids) {
    $pubs = entity_load('paragraphs_item', $pubids);
    foreach ($pubs as $item_id => $item) {
      echo 'processing publication to citation for ', $nid, "\n";
      // print_r($item->field_publication);
      // print_r($item->field_citation);
      if (empty($item->field_publication[LANGUAGE_NONE])) {
        echo 'no publications for ', $nid, ' - pass', "\n";
        continue;
      }
      if (!empty($item->field_citation[LANGUAGE_NONE])) {
        echo 'existing citations for ', $nid, ' - pass', "\n";
        continue;
      }
      foreach ($item->field_publication[LANGUAGE_NONE] as $delta => $row) {
        $crow = $row;
        $crow['format'] = 'filtered_html';
        $item->field_citation[LANGUAGE_NONE][] = $crow;
      }
      // updating this in code doesn't work - cheat
      $data_sql = <<<EOQ
insert into field_data_field_citation (
entity_type,
bundle,
deleted,
entity_id,
revision_id,
language,
delta,
field_citation_value,
field_citation_format
)
select
entity_type,
bundle,
deleted,
entity_id,
revision_id,
language,
delta,
field_publication_value,
'filtered_html'
from field_data_field_publication
where entity_type = 'paragraphs_item'
and bundle = 'publication'
and entity_id = {$item_id}
EOQ;
      db_query($data_sql);
      $revision_sql = <<<EOQ
insert into field_revision_field_citation (
entity_type,
bundle,
deleted,
entity_id,
revision_id,
language,
delta,
field_citation_value,
field_citation_format
)
select
entity_type,
bundle,
deleted,
entity_id,
revision_id,
language,
delta,
field_publication_value,
'filtered_html'
from field_revision_field_publication
where entity_type = 'paragraphs_item'
and bundle = 'publication'
and entity_id = {$item_id}
EOQ;
      db_query($revision_sql);
      echo 'copied publication to citation for ', $nid, "\n";
    }
  }
}
