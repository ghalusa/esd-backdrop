<?php

class FixLocations {
  public $locations = array();
  public $terms = array();
  public $existing = array();
  public $orphans = array();

  function process($name) {
    if (empty($this->terms[$name])) {
      echo 'no new term named ', $name, "\n";
      return;
    }
    $row =& $this->terms[$name];
    if (!empty($row['processed'])) {
      return;
    }
    if (empty($row['term'])) {
      echo 'no existing term found for ', $name, "\n";
      $item = array(
        'name' => $name,
        'vocabulary' => 'locations',
      );
    }
    else {
      $item = $row['term'];
    }
    $item['weight'] = $row['weight'];
    $parent = '[none]';
    if (empty($row['parent'])) {
      $item['parent'] = array(0);
    }
    else {
      $parent = $row['parent'];
      if (empty($this->existing[$parent])) {
        echo 'no existing term for ', $name, ' parent ', $parent, "\n";
        $this->process($parent);
        if (empty($this->terms[$parent]['processed']->tid)) {
            echo 'FAIL could not obtain parent tid for ', $name, ' parent ', $parent, "\n";
            return;
        }
        $item['parent'] = array($this->terms[$parent]['processed']->tid);
      }
      else {
        $item['parent'] = array($this->existing[$parent]->tid);
      }
    }
    $term = new TaxonomyTerm($item);
    if ($term) {
      $result = $term->save();
      if ($result) {
        echo 'updated/created term for ', $name, ' parent ', $parent, ' tid ', $term->tid, "\n";
        $row['processed'] = $term;
      }
      else {
        echo 'FAIL could not save term for ', $name, ' parent ', $parent, "\n";
      }
    }
    else {
      echo 'FAIL could not create term object for ', $name, ' parent ', $parent, "\n";
    }
  }
}

// read locations from text file
$locfile = drush_shift();
if (empty($locfile)) {
  echo 'no location file provided', "\n";
  return;
}

$fh = fopen(dirname(__FILE__) . '/' . $locfile, 'r');
if (!$fh) {
  echo 'unable to open file ', $locfile, ' here in ', getcwd(), "\n";
  return;
}

$fix = new FixLocations();

$parents = array('','','');
$plen = count($parents);
while (!feof($fh)) {
  $row = fgetcsv($fh, 2048);
  if (!$row) continue;
  $record = array_pad($row, $plen, '');
  $found = false;
  for ($i = 0; $i < $plen; $i++ ) {
    if (empty($record[$i])) {
      if ($found) {
        $parents[$i] = $record[$i];
      }
      else {
        $record[$i] = $parents[$i];
      }
    }
    else {
      $found = true;
      $parents[$i] = $record[$i];
    }
  }
  if (empty($record[0])) continue;
  if (empty($fix->locations[$record[0]])) {
    // echo 'set locations[', $record[0], '] to array()', "\n";
    $fix->locations[$record[0]] = array();
  }
  if (empty($record[1])) continue;
  if (empty($fix->locations[$record[0]][$record[1]])) {
    // echo 'set locations[', $record[0], '][', $record[1], '] to array()', "\n";
    $fix->locations[$record[0]][$record[1]] = array();
  }
  if (empty($record[2])) continue;
  if (empty($fix->locations[$record[0]][$record[1]][$record[2]])) {
    // echo 'set locations[', $record[0], '][', $record[1], '][', $record[2], '] to array()', "\n";
    $fix->locations[$record[0]][$record[1]][$record[2]] = array();
  }
}
unset($fix->locations['Global']);
ksort($fix->locations);
$fix->locations = array('Global' => array()) + $fix->locations;
$weight = 0;
foreach ($fix->locations as $zero => &$ones) {
  $fix->terms[$zero] = array('weight' => $weight++, 'parent' => '');
  if (empty($ones)) continue;
  ksort($ones);
  foreach ($ones as $one => &$twos) {
    $fix->terms[$one] = array('weight' => $weight++, 'parent' => $zero);
    if (empty($twos)) continue;
    ksort($twos);
    foreach ($twos as $two => &$threes) {
      $fix->terms[$two] = array('weight' => $weight++, 'parent' => $one);
      if (empty($threes)) continue;
      echo 'threes! '; var_dump($threes);
      ksort($threes);
    }
  }
}
// echo 'locations: '; print_r($fix->locations);
// echo 'terms: '; print_r($fix->terms);

// get existing locations
$tree = taxonomy_get_tree('locations');
// echo 'tree: '; print_r($tree);
foreach ($tree as $term) {
  if (empty($fix->existing[$term->name])) {
    $fix->existing[$term->name] = $term;
  }
  else {
    echo 'WARNING duplicate existing terms for ', $term->name, "\n";
    var_dump($fix->existing[$term->name], $term);
    continue;
  }
  if (empty($fix->terms[$term->name])) {
    // echo 'WARNING: no file match for existing term ', $term->name, "\n";
    $fix->orphans[$term->name] = (array)$term;
  }
  else {
    $fix->terms[$term->name]['term'] = (array)$term;
  }
}
// echo 'orphans: '; print_r($fix->orphans);
foreach (array_keys($fix->terms) as $name) {
  $fix->process($name);
}

