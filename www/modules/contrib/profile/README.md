Profile
========

Supports configurable user profiles.

Installation
-------------

 * Copy the whole profile directory to your modules directory and
   activate the module.


Usage
-----
   
 * Go to /admin/structure/profiles for managing profile types.
 * By default users may view their profile at /user and edit them at
   'user/X/edit'.

LICENSE
---------------    

This project is GPL v2 software. See the LICENSE.txt file in this directory 
for complete text.

Credits:
----------
Ported by docwlmot

Original Drupal maintained module by
 * Wolfgang Ziegler (fago), nuppla@zites.net
 * Joachim Noreiko (joachim), joachim.n+backdrop@gmail.com

